package com.waracle.cakemgr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.exception.CakeManagerException;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.core.IsEqual;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CakeServiceImplTest {

    private static final String CAKE_DATA = "src/main/resources/cake.json";

    @Mock
    private ObjectMapper objectMapperMock;
    @Mock
    private SessionFactory sessionFactoryMock;
    @Mock
    private Session sessionMock;
    @Mock
    private Criteria criteriaMock;
    @Mock
    private Transaction transactionMock;

    private CakeServiceImpl cakeServiceImpl;

    @Before
    public void setUp() {
        cakeServiceImpl = new CakeServiceImpl(objectMapperMock, sessionFactoryMock);
        given(sessionFactoryMock.openSession()).willReturn(sessionMock);
        when(sessionMock.createCriteria(CakeEntity.class)).thenReturn(criteriaMock);
        when(criteriaMock.add(any(Criterion.class))).thenReturn(criteriaMock);
        when(sessionMock.getTransaction()).thenReturn(transactionMock);
    }

    @Test
    public void shouldInitialiseCakes() throws Exception {
        //given
        given(objectMapperMock.readValue(any(InputStream.class), any(TypeReference.class))).willReturn(createCakeEntities());

        //when
        cakeServiceImpl.initialiseCakes(CAKE_DATA);

        //then
        verifyCakeEntity(5);
    }

    @Test (expected = IOException.class)
    public void shouldThrowIoExceptionWhenInitialiseCakes() throws Exception {
        //given
        doThrow(new IOException(EMPTY)).when(objectMapperMock).readValue(any(InputStream.class), any(TypeReference.class));

        //when
        cakeServiceImpl.initialiseCakes(CAKE_DATA);

        //then
    }

    @Test (expected = CakeManagerException.class)
    public void shouldThrowCakeManagerExceptionWhenInitialiseCakes() throws Exception {
        //given
        doThrow(new RuntimeException(EMPTY)).when(criteriaMock).add(any(Criterion.class));

        //when
        cakeServiceImpl.initialiseCakes(CAKE_DATA);

        //then
    }

    @Test
    public void shouldPersistCake() throws Exception {
        //given
        CakeEntity cakeEntity = getCakeEntityIntFunction().apply(1);

        //when
        cakeServiceImpl.persistCake(cakeEntity);

        //then
        verifyCakeEntity(1);
    }

    @Test
    public void shouldGetCakes() throws Exception {
        //given
        CakeEntity cakeEntity = getCakeEntityIntFunction().apply(1);

        given(sessionMock.createCriteria((Class) any())).willReturn(criteriaMock);
        given(criteriaMock.list()).willReturn(createCakeEntities());

        //when
        List<CakeEntity> cakeEntities = cakeServiceImpl.getCakes();

        //then
        assertThat("Cake entity list is incorrect", cakeEntities.size(), equalTo(5));
    }

    private List<CakeEntity> createCakeEntities() {
        return IntStream.range(0,5).mapToObj(getCakeEntityIntFunction()).collect(Collectors.toList());
    }

    private IntFunction<CakeEntity> getCakeEntityIntFunction() {
        return count -> {
            CakeEntity cakeEntity = new CakeEntity();
            cakeEntity.setTitle("title"+ count);
            cakeEntity.setDescription("Description"+ count);
            cakeEntity.setImage("Image" + count);
            return cakeEntity;
        };
    }

    private void verifyCakeEntity(int numberOfTimes) {
        verify(sessionMock, times(numberOfTimes)).beginTransaction();
        verify(sessionMock, times(numberOfTimes)).persist(any(CakeEntity.class));
        verify(sessionMock, times(numberOfTimes)).getTransaction();
    }
}
