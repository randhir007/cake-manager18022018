package com.waracle.cakemgr;

import com.waracle.cakemgr.exception.CakeManagerException;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.core.IsEqual;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CakeServletTest {

    private static final String ACCEPT = "accept";
    private static final String APPLICATION_JSON = "application/json";

    @Mock
    private CakeService cakeServiceMock;
    @Mock
    private HttpServletRequest requestMock;
    @Mock
    private HttpServletResponse responseMock;
    @Mock
    private PrintWriter printWriterMock;
    @Mock
    private ServletContext servletContextMock;
    @Mock
    private RequestDispatcher requestDispatcherMock;

    private CakeServlet cakeServlet;

    @Before
    public void setUp() {
        cakeServlet = new TestCakeServlet(cakeServiceMock);
    }

    @Test
    public void shouldInitialise() throws Exception {
        //given

        //when
        cakeServlet.init();

        //then
        verify(cakeServiceMock, times(1)).initialiseCakes(anyString());
    }

    @Test(expected = ServletException.class)
    public void shouldThrowCakeManagerException() throws Exception {
        //given
        doThrow(new CakeManagerException(EMPTY)).when(cakeServiceMock).initialiseCakes(anyString());

        //when
        cakeServlet.init();

        //then
    }

    @Test
    public void shouldPrintJsonWhenCallDoGet() throws Exception {
        //given
        given(requestMock.getHeader(ACCEPT)).willReturn(APPLICATION_JSON);
        given(responseMock.getWriter()).willReturn(printWriterMock);
        given(cakeServiceMock.getCakes()).willReturn(createCakes());

        //when
        cakeServlet.doGet(requestMock, responseMock);

        //then
        verify(printWriterMock).print("[{\"id\":null,\"title\":\"title\",\"image\":\"image\",\"desc\":\"description\"}]");
    }

    @Test
    public void shouldForwardWhenCallDoGet() throws Exception {
        //given
        given(requestMock.getHeader(ACCEPT)).willReturn("text/html");
        given(cakeServiceMock.getCakes()).willReturn(createCakes());
        when(servletContextMock.getRequestDispatcher("/index.jsp")).thenReturn(requestDispatcherMock);

        //when
        cakeServlet.doGet(requestMock, responseMock);

        //then
        verify(requestDispatcherMock, times(1)).forward(requestMock, responseMock);
    }

    @Test
    public void shouldCallDoPost() throws Exception {
        //given
        given(requestMock.getHeader(ACCEPT)).willReturn("text/html");
        given(cakeServiceMock.getCakes()).willReturn(createCakes());
        given(servletContextMock.getRequestDispatcher("/index.jsp")).willReturn(requestDispatcherMock);
        given(requestMock.getParameter("title")).willReturn("title");
        given(requestMock.getParameter("description")).willReturn("description");
        given(requestMock.getParameter("image")).willReturn("image");

        //when
        cakeServlet.doPost(requestMock, responseMock);

        //then
        verify(requestDispatcherMock, times(1)).forward(requestMock, responseMock);
        ArgumentCaptor<CakeEntity> peopleCaptor = ArgumentCaptor.forClass(CakeEntity.class);
        verify(cakeServiceMock).persistCake(peopleCaptor.capture());

        assertThat("Title is not valid", peopleCaptor.getValue().getTitle(), equalTo("title"));
        assertThat("Description is not valid", peopleCaptor.getValue().getDescription(), equalTo("description"));
        assertThat("Image is not valid", peopleCaptor.getValue().getImage(), equalTo("image"));
    }

    private class TestCakeServlet extends CakeServlet {
        TestCakeServlet(CakeService cakeService) {
            super(cakeService);
        }
        public ServletContext getServletContext() {
            return servletContextMock;
        }
    }

    private List<CakeEntity> createCakes() {
        CakeEntity cakeEntity = new CakeEntity();
        cakeEntity.setTitle("title");
        cakeEntity.setDescription("description");
        cakeEntity.setImage("image");
        return Collections.singletonList(cakeEntity);
    }
}
