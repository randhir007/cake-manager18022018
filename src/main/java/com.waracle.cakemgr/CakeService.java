package com.waracle.cakemgr;

import com.waracle.cakemgr.exception.CakeManagerException;

import java.io.IOException;
import java.util.List;

public interface CakeService {

    void initialiseCakes(String path) throws CakeManagerException, IOException;

    void persistCake(final CakeEntity cakeEntity);

    List<CakeEntity> getCakes();
}
