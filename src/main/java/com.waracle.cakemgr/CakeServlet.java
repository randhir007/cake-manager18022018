package com.waracle.cakemgr;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.exception.CakeManagerException;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

@WebServlet(urlPatterns = {"/", "/cakes"}, loadOnStartup = 1)
public class CakeServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(CakeServlet.class);
    private static final String CAKE_DATA = "src/main/resources/cake.json";
    private static final String INDEX_PAGE = "/index.jsp";
    private static final String APPLICATION_JSON = "application/json";
    private static final String ACCEPT = "accept";
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String IMAGE = "image";
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private CakeService cakeService;

    public CakeServlet() {
        this(new CakeServiceImpl());
    }

    public CakeServlet(CakeService cakeService) {
        super();
        this.cakeService = cakeService;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        LOGGER.info("init started");
        try {
            cakeService.initialiseCakes(CAKE_DATA);
        } catch (CakeManagerException | IOException ex) {
            throw new ServletException(ex);
        }
        LOGGER.info("init finished");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("GET Request /cakes");
        if (request.getHeader(ACCEPT).contains(APPLICATION_JSON)) {
            response.getWriter().print(cakesAsJson());
        } else {
            request.setAttribute("cakes", cakeService.getCakes());
            getServletContext().getRequestDispatcher(INDEX_PAGE).forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("POST Request /cakes");
        validateRequest(request);
        cakeService.persistCake(parseRequest(request));
        doGet(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
        HibernateUtil.shutdown();
    }

    private void validateRequest(HttpServletRequest request) throws ServletException {
        if (StringUtils.isBlank(request.getParameter(TITLE))) {
            throw new ServletException("Title is not valid");
        }
    }

    private CakeEntity parseRequest(HttpServletRequest request) {
        CakeEntity cakeEntity = new CakeEntity();
        cakeEntity.setTitle(request.getParameter(TITLE));
        cakeEntity.setDescription(request.getParameter(DESCRIPTION));
        cakeEntity.setImage(request.getParameter(IMAGE));
        return cakeEntity;
    }

    private String cakesAsJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(cakeService.getCakes());
    }
}
