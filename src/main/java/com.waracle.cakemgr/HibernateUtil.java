package com.waracle.cakemgr;

import com.waracle.cakemgr.exception.CakeManagerException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class HibernateUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(CakeServiceImpl.class);

    private static SessionFactory sessionFactory = buildSessionFactory();

    private HibernateUtil() throws CakeManagerException {
        //Throwing exception if anyone try to create instance using reflection.
        throw new CakeManagerException("HibernateUtil sessionFactory already exist. Creating an instance is disabled.");
    }

    private static SessionFactory buildSessionFactory() {
        LOGGER.info("HibernateUtil buildSessionFactory called");
        try {
            if (sessionFactory == null) {
                Configuration configuration = new Configuration().configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));
                StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
                serviceRegistryBuilder.applySettings(configuration.getProperties());
                ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            }
            return sessionFactory;
        } catch (Throwable ex) {
            LOGGER.error("Error while building session factory: {}", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        LOGGER.debug("Called HibernateUtil getSessionFactory");
        return sessionFactory;
    }

    public static void shutdown() {
        LOGGER.debug("Called shutdown");
        getSessionFactory().close();
    }
}
