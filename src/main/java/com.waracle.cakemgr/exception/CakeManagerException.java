package com.waracle.cakemgr.exception;

import javax.servlet.ServletException;

public class CakeManagerException extends Exception {

    private static final long serialVersionUID = 5400022829215898651L;

    public CakeManagerException(String message) {
        super(message);
    }
}


