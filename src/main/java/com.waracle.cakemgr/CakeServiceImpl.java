package com.waracle.cakemgr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.exception.CakeManagerException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CakeServiceImpl implements CakeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CakeServiceImpl.class);
    private static final String TITLE = "title";

    private ObjectMapper objectMapper;
    private SessionFactory sessionFactory;

    public CakeServiceImpl () {
        this(new ObjectMapper(), HibernateUtil.getSessionFactory());
    }

    public CakeServiceImpl(ObjectMapper objectMapper, SessionFactory sessionFactory) {
        this.objectMapper = objectMapper;
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void initialiseCakes(String path) throws CakeManagerException, IOException {
        LOGGER.debug("Cake data path is {}", path);
        try(InputStream inputStream = new FileInputStream(path)) {
            List<CakeEntity> cakes = objectMapper.readValue(inputStream, new TypeReference<List<CakeEntity>>(){});
            persistCakes(cakes);
        } catch (IOException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new CakeManagerException(ex.getMessage());
        }
    }

    @Override
    public void persistCake(final CakeEntity cakeEntity) {
        Session session = sessionFactory.openSession();
        try {
            CakeEntity updatedCake = getUpdatedCakeIfAlreadyExist(cakeEntity, session);
            session.beginTransaction();
            session.persist(updatedCake);
            session.getTransaction().commit();
            LOGGER.debug("Cake stored. title {}", updatedCake.getTitle());
        } finally {
            session.close();
        }
    }

    @Override
    public List<CakeEntity> getCakes() {
        Session session = sessionFactory.openSession();
        try {
            return session.createCriteria(CakeEntity.class).list();
        } finally {
            session.close();
        }
    }

    private void persistCakes(List<CakeEntity> cakeEntities) {
        cakeEntities.stream().forEach(this::persistCake);
    }

    private CakeEntity getUpdatedCakeIfAlreadyExist(final CakeEntity cakeEntity, Session session) {
        List<CakeEntity> cakeEntities = session.createCriteria(CakeEntity.class).add(Restrictions.eq(TITLE, cakeEntity.getTitle())).list();
        if (!cakeEntities.isEmpty()) {
            CakeEntity existingCake = cakeEntities.get(0);
            LOGGER.debug("Cake already exist. Title {}", existingCake.getTitle());
            existingCake.setTitle(cakeEntity.getTitle());
            existingCake.setDescription(cakeEntity.getDescription());
            existingCake.setImage(cakeEntity.getImage());
            return existingCake;
        }
        LOGGER.debug("Cake with title {} to be persisted", cakeEntity.getTitle());
        return cakeEntity;
    }
}
