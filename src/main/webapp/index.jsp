<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
<title>Cake Manager</title>
<meta charset="utf-8">
<style>
     <%@ include file="css/cakestyle.css"%>
</style>
</head>
<body>
    <h1 align="center">Cake Manager</h1>
    <div>
        <form action="/cakes" method="post">
            <table>
                <tr>
                <td colspan="3"><b>Add cake to store</b></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><input type="text" name="title" id="title"/>
                </tr>
                <tr>
                    <td>Description</td>
                    <td> <input type="text" name="description" id="description" /></td>
                </tr>
                <tr>
                    <td>Image link</td>
                    <td><input type="text" name="image" id="image"/></td>
                <tr>
                    <td colspan="3"><input type="submit" />
                </tr>
            </table>
        </form>
    </div>
    <center><h1>Cake available in store bakery</h1></center>
    <div>
    <table>
        <thead>
            <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            </tr>
        </thead>
        <tbody>
           <c:forEach var="cake" items="${cakes}">
            <tr>
               <td>${cake.title}</td>
               <td>${cake.description}</td>
               <td><img src="${cake.image}" style="height: 200px;width:200px;"/></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    </div>
</body>
</html>